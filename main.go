package golangenum

import "fmt"

type Enum[T comparable] struct {
	m    []T // slice of matching elements
	Name string
}

// Create new enum
func (e *Enum[T]) New(name string, enums ...T) {
	e.m = enums
	e.Name = name
}

// Add new matching value to enum
func (e *Enum[T]) Add(v T) {
	// keep it unique
	if e.IsValid(v) {
		panic(fmt.Sprintf("enum type can contain only unique values, value: %v duplicated for %v", v, e.Name))
	}
	e.m = append(e.m, v)
}

func (e *Enum[T]) IsValid(v T) bool {
	idx := e.index(v)
	return idx != -1
}

// Checks if the specified value is one of the listed
func (e *Enum[T]) Check(v T) error {
	if !e.IsValid(v) {
		return fmt.Errorf("invalid value: %v for enum %v", v, e.Name)
	}
	return nil // valid value
}

// Remove by given vale, keep slice order
func (e *Enum[T]) Remove(v T) {
	if idx := e.index(v); idx != -1 {
		e.removeIndex(idx)
	}
}

func (e *Enum[T]) removeIndex(idx int) {
	e.checkIndex(idx)
	e.m = append(e.m[:idx], e.m[1+idx:]...)
}

func (e *Enum[T]) checkIndex(idx int) {
	if idx > len(e.m) {
		panic(fmt.Sprintf("index: %v ouf of range 0 - %v", idx, len(e.m)))
	}
}

// Get index of enum by value
func (e Enum[T]) index(v T) int {
	for k, enum := range e.m {
		if v == enum {
			return k
		}
	}
	return -1
}
